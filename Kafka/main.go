package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"github.com/segmentio/kafka-go"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB
var err error

type Frontend struct {
	Issue string `json:"issue"`
}
type Backend struct {
	Issue string `json:"issue"`
}
type Responseback struct {
	Issue string `json:"issue"`
	Type  string  `json:"type"`
}
type Displayback struct {
	Back    []Backend `json:"dat"`
	Present bool      `json:"present"`
}
type Displayfront struct {
	Back    []Frontend `json:"dat"`
	Present bool      `json:"present"`
}
type Feedback struct {
	FeedbackID    uuid.UUID `json:"id"`
	FeedbackType  string    `json:"type"`
	FeedbackIssue string    `json:"issue"`
}
type response struct {
	Res string `json:"issue"`
}

const (
	topic          = "FrontIssueKafka"
	broker1Address = "localhost:9092"
)

var record Feedback
var respo response

func init() {
	dsn := "host=localhost user=postgres password=root dbname=postgres port=5432 sslmode=disable TimeZone=Asia/Kolkata"
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	fmt.Println("Now we are connected to POSTGRESQL DATABASE.")
}

func TakeData(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := context.Background()
	fmt.Print("started Server")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&record)
	record.FeedbackID = uuid.New()
	if record.FeedbackType == "Frontend" {
		go produceFront(ctx)
		consumeFront(ctx)
	} else {
		go produceBack(ctx)
		consumeBack(ctx)
	}

}
func produceBack(ctx context.Context) {

	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{broker1Address},
		Topic:   "BackIssueKafka",
	})
	err := w.WriteMessages(ctx, kafka.Message{
		Key:   []byte("feedback "),
		Value: []byte(record.FeedbackIssue),
	})
	if err != nil {
		panic("could not write message " + err.Error())
	}
}
func produceFront(ctx context.Context) {

	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{broker1Address},
		Topic:   topic,
	})
	err := w.WriteMessages(ctx, kafka.Message{
		Key:   []byte("feedback "),
		Value: []byte(record.FeedbackIssue),
	})
	if err != nil {
		panic("could not write message " + err.Error())
	}
}
func consumeBack(ctx context.Context) {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{broker1Address},
		Topic:   "BackIssueKafka",
		GroupID: "my-group",
	})
	rec := Backend{}
	n, err := r.ReadMessage(ctx)
	if err != nil {
		panic("could not read message " + err.Error())
	}
	rec.Issue = string(n.Value)
	result := db.Create(&rec)
	if result.Error != nil {
		panic(result.Error)
	}
}
func consumeFront(ctx context.Context) {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{broker1Address},
		Topic:   topic,
		GroupID: "my-group",
	})
	rec := Frontend{}
	n, err := r.ReadMessage(ctx)
	if err != nil {
		panic("could not read message " + err.Error())
	}
	rec.Issue = string(n.Value)
	result := db.Create(&rec)
	if result.Error != nil {
		panic(result.Error)
	}
}
func orderShowFront(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	
	fmt.Println("Got Post Request from  the Getorders server....")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	var AllIssues []Frontend

	result := db.Find(&AllIssues)
	if result.Error != nil {
		panic(result.Error)
	}
	res := Displayfront{}
	fmt.Println(result.RowsAffected)
	if result.RowsAffected == 0 {
		res.Present = false
		json.NewEncoder(w).Encode(res)
		return
	} else {
		res.Back = AllIssues
		res.Present = true
		json.NewEncoder(w).Encode(res)
		return

	}

}
func orderShowBack(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	fmt.Println("Got Post Request from  the Getorders server....")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	var AllIssues []Backend

	result := db.Find(&AllIssues)
	if result.Error != nil {
		panic(result.Error)
	}
	res := Displayback{}
	if result.RowsAffected == 0 {
		res.Present = false
		json.NewEncoder(w).Encode(res)
		return
	} else {
		res.Back = AllIssues
		res.Present = true
		json.NewEncoder(w).Encode(res)
		return

	}

}
func del(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//fmt.Println("Got Post Request from  the Getorders server....")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	respo:=Responseback{}
	json.NewDecoder(r.Body).Decode(&respo)
	if(respo.Type == "backend"){
		result := db.Where("issue = ?", respo.Issue).Delete(&Backend{})
	if result.Error != nil {
		panic(result.Error)
	}
	}else{
	result := db.Where("issue = ?", respo.Issue).Delete(&Frontend{})
	if result.Error != nil {
		panic(result.Error)
	}
}
}
func main() {
	router := httprouter.New()
	router.POST("/produce", TakeData)
	router.GET("/orders", orderShowFront)
	router.GET("/ordersBack", orderShowBack)
	router.POST("/del", del)
	log.Fatal(http.ListenAndServe(":8000", router))

}

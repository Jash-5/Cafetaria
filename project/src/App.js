import React,{lazy,Suspense} from "react"
import './App.css';
import Footer from "./Components/Footer"
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./Components/Login.js"
import Signup from "./Components/Signup.js"
import About from "./Components/About.js"
import Create from "./Components/Create";
import Forgot from "./Components/Forgot";
import 'react-notifications-component/dist/theme.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Myorders from "./Components/Myorders";
import Logout from "./Components/Logout.js";
import Cart from "./Components/Cart.js";
import Feedback from "./Components/Feedback";
import Thanks from "./Components/Thanks";
const Inventory=lazy(()=>import("./Components/Inventory.js"));
function App() {
 localStorage.setItem('Burger',15)
 localStorage.setItem('IceCream',5)
 localStorage.setItem('ChocolateMilkshake',5)
 localStorage.setItem('Donut',3)
 localStorage.setItem('FrenchFries',3)
 localStorage.setItem('pasta',18)
 localStorage.setItem('Pizza',15)
 localStorage.setItem('Sandwich',15)
 localStorage.setItem('SoftDrinks',3)
 localStorage.setItem('Pastry',15)
 localStorage.setItem('Noodles',20)
 localStorage.setItem('Coffee',2)
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" render={() => {
            return (
              <Suspense fallback={<div className="load">Loading...</div>}>
              <div className="jash">
                <Inventory />
              </div>
              </Suspense>
            )
          }}>
          </Route>
          <Route exact path="/feedback">
            <Feedback />
            </Route>
          <Route exact path="/cart">
            <Cart />
            </Route>
          <Route exact path="/order">
            <Myorders />
          </Route>
          <Route exact path="/about">
            <About />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/signup">
            <Signup />
          </Route>
          <Route exact path="/forgot">
            <Forgot />
          </Route>
          <Route exact path="/logout">
            <Logout />
          </Route>
          <Route exact path="/forgot/create">
            <Create />
          </Route>
          <Route exact path="/thanks">
            <Thanks />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </div>
    
  );
}


export default App;

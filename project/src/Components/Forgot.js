import React,{useState} from 'react'
import { Form, Button } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import Header from "./Header.js"
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import './Login.css'

function Forgot() {
    let history=useHistory()
    const [data, setData] = useState(
        {
            username: "",
            game: ""
        }
    )
    function saveData(e){

   fetch("http://localhost:8080/forgot", {
    method: "POST",
    headers: { 
      'Accept':'application/json',
      'Content-Type': 'application/x-www-form-urlencoded' 
    },
    body: JSON.stringify(data)
  }).then(function(response){
     return response.json()
  })
  .then(function(myJson) {
    if(myJson.present===true)
    {
      localStorage.setItem('ID',myJson.id)  
      history.push('/forgot/create');  
         
    }
    else{
      NotificationManager.error('New User Try Signup',"Wrong",700);
      history.push('/forgot')
    }
  });
  e.preventDefault()
    }
    function handle(e) {
        const newdata = { ...data }
        newdata[e.target.id] = e.target.value
        setData(newdata)
    }
    return (

        <>
            <Header />
            <NotificationContainer />
            <div className='FormStyle container my-5'>
                <Form onSubmit={saveData}>
                    <Form.Group className="mb-3" controlId="username">
                        <Form.Label>UserName</Form.Label>
                        <Form.Control type="text" minLength="5" maxLength="30" required onChange={(e) => handle(e)} value={data.username} placeholder="Enter usename" />
                    </Form.Group>

        

                    <Form.Group className="mb-3" controlId="game">
                        <Form.Label>Favourite Game</Form.Label>
                        <Form.Control type="text" minLength="2" maxLength="30" required onChange={(e) => handle(e)} value={data.game} placeholder="..." />
                    </Form.Group>
                    <Button className="styleButton" variant="success" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>

        </>
    )
}

export default Forgot
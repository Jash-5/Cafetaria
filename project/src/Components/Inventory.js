import React from 'react'
import { Card, Button, Container, Row, Col } from "react-bootstrap"
import Images from "./Images"
import './inventory.css'
import {useHistory} from 'react-router-dom'
import Header from "./Header.js"
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

function Inventory() {
  let history=useHistory();

  function addToCart(e)
  {
    if(localStorage.getItem('ID')==null)
    {
      history.push('/login')
    }
    else{
    var id=localStorage.getItem('ID');
    if(localStorage.getItem(id)==null)
    {
      localStorage.setItem(id,e)
    }
    else{
    localStorage.setItem(id,e+" "+localStorage.getItem(id))
    }
    NotificationManager.success('Succesfully Added To the Cart','Added',700);
    }

  }
  return (
    <> 
    <Header/>
    <NotificationContainer/>
    <div className=' container '>
      <Container >
        <Row className="py-3" >
          <Col className="my-1 " xs={12} md={6}  lg={3}>

            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image1} alt="Burger Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Burger</strong></Card.Title>
                <Card.Text>
                  Cost : 15$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"Burger")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>

          </Col>
          <Col className="my-1  " xs={12} md={6}  lg={3}>
            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image2} alt="Ice Cream Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc'}} >
                <Card.Title><strong>Ice Cream</strong></Card.Title>
                <Card.Text>
                  Cost : 5$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"IceCream")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>

          <Col className="my-1  " xs={12} md={6}  lg={3}>
            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image3} alt="Chocolate Milkshake Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Chocolate Milkshake</strong></Card.Title>
                <Card.Text>
                  Cost : 5$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"ChocolateMilkshake")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>

          <Col className="my-1  " xs={12} md={6}  lg={3}>
            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image4} alt="DonutI mage" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Donut</strong></Card.Title>
                <Card.Text>
                  Cost : 3$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"Donut")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <Row className="py-3">
          <Col className="my-1 " xs={12} md={6}  lg={3}>
            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img variant="top" src={Images.image5} alt="French Fries Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>French Fries</strong></Card.Title>
                <Card.Text>
                  Cost : 3$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"FrenchFries")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>
          <Col className="my-1  " xs={12} md={6}  lg={3}>

            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image7} alt="Pasta Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Pasta</strong></Card.Title>
                <Card.Text>
                  Cost : 18$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"pasta")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>
          <Col className="my-1  " xs={12} md={6}  lg={3}>

            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image8} alt="Pizza Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Cheese Pizza</strong></Card.Title>
                <Card.Text>
                  Cost : 15$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"Pizza")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>
          <Col className="my-1  " xs={12} md={6}  lg={3}>

            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image9} alt="Sandwich Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Sandwich</strong></Card.Title>
                <Card.Text>
                  Cost : 15$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"Sandwich")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <Row className="py-3">
          <Col className="my-1  " xs={12} md={6}  lg={3}>

            <Card className="item" tyle={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image6} alt="Cappuccino Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Capuccino Coffe</strong></Card.Title>
                <Card.Text>
                  Cost : 2$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"Coffee")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>
          <Col className="my-1  " xs={12} md={6}  lg={3}>

            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image10} alt="SoftDrinks Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Soft Drinks</strong></Card.Title>
                <Card.Text>
                  Cost : 3$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"SoftDrinks")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>

          <Col className="my-1  " xs={12} md={6}  lg={3}>
            <Card className="item"  style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image11} alt="Pastry Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Pastry</strong></Card.Title>
                <Card.Text>
                  Cost : 15$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"Pastry")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>

          <Col className="my-1  " xs={12} md={6}  lg={3}>
            <Card className="item" style={{ width: '15rem' }}>
              <Card.Img  variant="top" src={Images.image12} alt="Noodles Image" />
              <Card.Body style={{ backgroundColor:'#ffe5cc' }} >
                <Card.Title><strong>Noodles</strong></Card.Title>
                <Card.Text>
                  Cost : 20$
                  <br></br>
                </Card.Text>
                <Button className="styleButton" onClick={addToCart.bind(this,"Noodles")} variant="primary btn btn-success" id="button">Add to Cart</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>


      </Container>



    </div>
    </>
  )
}

export default Inventory
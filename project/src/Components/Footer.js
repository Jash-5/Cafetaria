import React from 'react'
//mport { Container } from 'react-bootstrap'
import { FaEnvelope, FaPhone, FaTwitter, FaFacebook, FaInstagram } from "react-icons/fa"
import './Footer.css'
//import Header from "./Header.js"
function Footer() {
  return (
    <>
      <div className="bg-dark ok" >
        <div className='centre-block'>
          <div className="text-center " style={{ color: 'white' }}>
            <p>&copy;Copyright Jash &nbsp;&nbsp;<FaEnvelope /> &nbsp;&nbsp;lynkcafetaria@lynk.co.in &nbsp;&nbsp;  <FaPhone />&nbsp;&nbsp; +919959585764</p>
            <p>Follow us on Twitter &nbsp; <a href="https://twitter.com/lynklogistics?lang=en"><FaTwitter /></a>&nbsp;&nbsp;Facebook&nbsp;&nbsp;<a href="https://www.facebook.com/lynk.india/"><FaFacebook /></a>&nbsp;&nbsp;and Instagram&nbsp;&nbsp;<a href="https://www.instagram.com/lynk_logistics/?hl=en"><FaInstagram /></a>
            </p>
          </div>
        </div>
      </div>
    </>

  )
}

export default Footer